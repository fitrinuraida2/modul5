import java.awt.*;
import java.awt.event.*;
public class MouseEventDemo extends Frame implements MouseListener,
MouseMotionListener {
	TextField tf;
	public MouseEventsDemo (String title){
		super(title);
		tf = new TextField(60);
		addMouseListener(this);
	}
	public void launchFrame(){
		/*Menambah komponen pada framw */
		add(tf, BorderLayout.SOUTH);
		setSize(300,300);
		setVisible(true);
	}
	public void mouseClicked(MouseEvent me){
 		String msg = "Mouse clicked.";
 		tf.setText(msg);
	}
	publuc void mouseEntered (MouseEvent me){
		String msg ="Mouse entered component.";
		tf.setText(msg);

		//dan seterusnya lengkapi untuk oberiding methodnya
		public static void main (Stirng args []){
			MouseEventDemo med = new MouseEventsDemo("Mouse Events Demo");
			med.launchFrame();
		}
	}
}
