import java.awt.*;
import java.awt.event.*;

public class CloseFrame3 extends Frame{
    Label label;
    
    public CloseFrame3(String title){
        super(title);
        label = new Label("Tutup Frame!");
        this.addWindowListener(new WindowAdapter(){
        public void windowClosing(WindowEvent e){
            dispose();
            System.exit(1);
       }
    });
    }

    public void launchFrame(){
        setSize(300,300);
        setVisible(true);
    }
   
    public static void main(String[] args){
        CloseFrame3 close = new CloseFrame3("Close With AnonymClass");
        close.launchFrame();
    }
}
