
///// kelas TicTacToeGame turunan dari kelas GameObjects
public class TicTacToeGame extends GameObjects{
    
    ///// konstruktor TicTacToeGame
    public TicTacToeGame(String title) {
        ///// konstruktor super class
        super(title);
        ///// set seluruh komponen
        setComponent();
        ///// panggil eventComponent
        eventComponent();
    }
    
    public static void main(String[] args) {
        ///// memberi nilai konstruktor dan memanggil method launchFrame
        new TicTacToeGame("Tic Tac Toe").launchFrame();
    }
}